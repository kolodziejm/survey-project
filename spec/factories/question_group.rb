FactoryGirl.define do
  factory :question_group do
    title 'Title'
    position 0

    survey

    after(:build) do |question_group|
      question_group.questions = [FactoryGirl.build(:text_question, question_group: question_group)]
    end
  end
end