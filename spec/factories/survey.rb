FactoryGirl.define do
  factory :survey do
    title 'Title'

    owner { FactoryGirl.create(:survey_owner) }

    after(:build) do |survey|
      survey.question_groups = [FactoryGirl.build(:question_group, survey: survey)] if survey.question_groups.count==0
    end
  end
end