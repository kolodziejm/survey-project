FactoryGirl.define do
  factory :group do
    sequence :name do |n|
      "super_group_#{n}"
    end
  end
end