FactoryGirl.define do
  trait :user do
    email    { Forgery(:internet).email_address }
    password { Forgery(:basic).password(at_least: 8) }
    password_confirmation { password }
  end

  factory :user, traits: [:user] do
    type 'User'
  end

  factory :admin_user, traits: [:user] do
    type 'AdminUser'
  end

  factory :survey_owner, traits: [:user] do
    type 'SurveyOwner'
  end
end