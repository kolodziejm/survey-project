FactoryGirl.define do
  factory :question do
    text 'Question'
    position 0
    question_group

    trait :text do
      question_type :text
    end

    factory :text_question, traits: [:text]
  end
end