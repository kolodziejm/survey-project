FactoryGirl.define do
  factory :option do
    sequence(:text) { |n| "Option #{n}" }
    sequence(:position) { |n| n }
  end
end