require 'spec_helper'

describe Question do
  context 'validation' do
    let(:user) { FactoryGirl.create(:user) }
    let(:survey) { FactoryGirl.create(:survey) }

    context 'options count' do
      context 'single choice' do
        let(:question_type) { :single_choice }
        let!(:good_question) { FactoryGirl.build(:question, question_type: question_type, options: FactoryGirl.build_list(:option, 2)) }
        let!(:bad_questions) do
          [
            FactoryGirl.build(:question, question_type: question_type, options: []),
            FactoryGirl.build(:question, question_type: question_type, options: FactoryGirl.build_list(:option, 1))
          ]
        end

        it 'returns validation error if options count is less than 2' do
          bad_questions.each do |q|
            expect(q.valid?).to eq(false)
            expect(q.errors[:options]).to be_present
          end

          expect(good_question.valid?).to eq(true)
        end
      end

      context 'multiple choice' do
        let(:question_type) { :multiple_choice }
        let!(:good_question) { FactoryGirl.build(:question, question_type: question_type, options: FactoryGirl.build_list(:option, 1)) }
        let!(:bad_question) { FactoryGirl.build(:question, question_type: question_type, options: []) }

        it 'returns validation error if options count is less than 1' do
          expect(bad_question.valid?).to eq(false)
          expect(bad_question.errors[:options]).to be_present

          expect(good_question.valid?).to eq(true)
        end
      end
    end
  end
end