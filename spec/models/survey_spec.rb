require 'spec_helper'

describe Survey do
  context 'privacy options' do
    let(:survey) { FactoryGirl.build(:survey) }
    let(:user) { FactoryGirl.create(:user) }

    context 'when survey is public' do

      before { survey.public = true }

      it 'allows everyone' do
        expect(survey.answerable_by?(user)).to eq(true)
      end
    end

    context 'when survey is private' do
      before { survey.public = false }

      context 'user is in allowed users' do
        before { survey.allowed_users << user }

        it 'allows him' do
          expect(survey.answerable_by?(user)).to eq(true)
        end
      end

      context 'user is in allowed groups' do
        before do
          user.groups = [FactoryGirl.create(:group)]
          survey.allowed_groups << user.groups.first
        end

        it 'allows him' do
          expect(survey.answerable_by?(user)).to eq(true)
        end
      end

      context 'user is not in allowed users and not in allowed groups' do
        it 'disallows him' do
          expect(survey.answerable_by?(user)).to eq(false)
        end
      end
    end
  end

  context 'answerable_by scope' do
    let(:user) { FactoryGirl.create(:user, groups: [FactoryGirl.create(:group)]) }
    let(:other_user) { FactoryGirl.create(:user, groups: [FactoryGirl.create(:group)]) }

    let!(:good_surveys) do
      [
        FactoryGirl.create(:survey, public: true, title: 'Public'),
        FactoryGirl.create(:survey, public: false, users: [user], title: 'User'),
        FactoryGirl.create(:survey, public: false, groups: user.groups, title: 'Group'),
        FactoryGirl.create(:survey, public: false, users: [user], groups: user.groups, title: 'User&Group')
      ]
    end

    let!(:bad_surveys) do
      [
        FactoryGirl.create(:survey, public: false),
        FactoryGirl.create(:survey, public: false, users: [other_user]),
        FactoryGirl.create(:survey, public: false, groups: other_user.groups),
        FactoryGirl.create(:survey, public: false, users: [other_user], groups: other_user.groups)
      ]
    end

    it 'finds only proper surveys' do
      expect(described_class.answerable_by(user)).to match_array(good_surveys)
    end
  end

  context 'editable_by scope' do
    context 'normal user' do
      let(:owner) { FactoryGirl.create(:user) }
      let(:other_user) { FactoryGirl.create(:user) }
      let(:admin_user) { FactoryGirl.create(:admin_user) }
      let(:survey) { FactoryGirl.create(:survey, public: true, owner: owner) }

      it 'grants access for owner' do
        expect(survey.editable_by?(owner)).to eq(true)
      end

      it 'grants access for admin user' do
        expect(survey.editable_by?(admin_user)).to eq(true)
      end

      it 'rejects access for other users' do
        expect(survey.editable_by?(other_user)).to eq(false)
      end
    end
  end

  context 'order_by scope' do
    let!(:owner1) { FactoryGirl.create(:user, email: 'email3@email.com') }
    let!(:owner2) { FactoryGirl.create(:user, email: 'email1@email.com') }
    let!(:owner3) { FactoryGirl.create(:user, email: 'email2@email.com') }
    let!(:survey1) { FactoryGirl.create(:survey, public: true, owner: owner1, title: '3') }
    let!(:survey2) { FactoryGirl.create(:survey, public: true, owner: owner2, title: '1') }
    let!(:survey3) { FactoryGirl.create(:survey, public: true, owner: owner3, title: '2') }

    it 'sorts by basic survey attributes' do
      ordered = Survey.order_by(:title, :desc)

      expect(ordered.first).to eq(survey1)
      expect(ordered.second).to eq(survey3)
      expect(ordered.third).to eq(survey2)
    end

    it 'sorts by owner\'s email' do
      ordered = Survey.order_by(:owner_email, :asc)

      expect(ordered.first).to eq(survey2)
      expect(ordered.second).to eq(survey3)
      expect(ordered.third).to eq(survey1)
    end
  end
end