require 'spec_helper'

describe Group do
  context 'association with users' do
    let!(:group) { FactoryGirl.create(:group) }
    let!(:user)  { FactoryGirl.create(:user, groups: [group] ) }

    it 'adds user to group' do
      expect(group.users.count).to eq(1)
    end

    it 'removes associations with user after delete' do
      expect { group.delete }.to change { user.groups.count }.by -1
    end
  end
end