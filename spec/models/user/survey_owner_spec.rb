require 'spec_helper'

describe SurveyOwner do
  let!(:survey_owner) { FactoryGirl.create(:survey_owner) }

  it 'gets found by SurveyOwner class' do
    expect(SurveyOwner.count).to eq(1)
  end
end