require 'spec_helper'

describe User do
  context 'association with groups' do
    let!(:user)  { FactoryGirl.create(:user) }
    let!(:group) { FactoryGirl.create(:group, users: [user]) }

    it 'removes associations with group after delete' do
      expect { user.delete }.to change { group.users.count }.by -1
    end
  end
end