require 'spec_helper'

describe AdminUser do
  let!(:admin_user) { FactoryGirl.create(:admin_user) }

  it 'gets found by AdminUser class' do
    expect(AdminUser.count).to eq(1)
  end
end