require 'spec_helper'

describe Admin::UsersController do
  before do
    sign_in :user, FactoryGirl.create(:admin_user)
  end
  context 'updating' do
    [:user, :survey_owner, :admin_user].each do |user|
      context user.to_s do
        let!(:_user) { FactoryGirl.create(user) }
        let(:user_params) do
          { id: _user.id, user => { first_name: 'Bubu' } }
        end

        it "updates the #{user}" do
          put :update, user_params

          expect(_user.reload.first_name).to eq('Bubu')
        end
      end
    end
  end

  context 'deleting' do
    let!(:user) { FactoryGirl.create(:user) }
    it 'deletes the user' do
      expect do
        delete :destroy, id: user.id
      end.to change(User, :count).by -1
    end
  end
end