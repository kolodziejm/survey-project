module AnswersHelper
  def option_text(option)
    option_answer = @answer.option_answers.find_by_option_id(option.id)
    if option_answer
      text = option.custom? ? t('answers.show.other') + (option_answer.custom_text || '-'): option.text
      content_tag(:strong, text)
    else
      option.text
    end
  end
end