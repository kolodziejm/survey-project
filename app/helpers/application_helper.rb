module ApplicationHelper
  def page_title
    @page_title ||= I18n.t("title.#{full_controller_name}.#{action_name}")
  end

  def sort_link(title, column)
    sort_dir = sort_order.to_sym
    is_active_column = column.to_sym == sort_field.to_sym
    arrow = if is_active_column
              sort_dir == :asc ? '&#x25B2;' : '&#x25BC;'
            end

    header = is_active_column ? "#{title} #{arrow}" : title
    header = header.html_safe
    link_to header, request.parameters.merge({ sort_field: column, sort_order: sort_dir == :asc ? :desc : :asc })
  end

  def user_display_email(user)
    user.deleted_user? ? t('deleted_user') : user.email
  end

  private

  def full_controller_name
    controller_path.match(/admin/) ? "admin.#{controller_name}" : controller_name
  end

  def page_header(title, options = {})
    if options[:tooltip]
      content_tag :div, class: "page-header clearfix" do
        [
          content_tag(:h1, title, class: "fl"),
          content_tag(
            :span, '',
            class: "fr fa fa-info-circle fa-2x has-tooltip header-tooltip",
            data: { toggle: "tooltip", placement: options[:tooltip][:placement] || "left" },
            title: options[:tooltip][:title])
        ].join.html_safe
      end
    end
  end
end