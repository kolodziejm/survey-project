module SurveysHelper
  def activity_dates_subtitle(survey)
  	now = DateTime.now
    text = ''
    html_class = 'survey-subtitle'
  	if survey.active_since.present? && survey.active_since > now
  		text = t('surveys.show.survey_starts_at', date: survey.active_since)
      html_class += ' inactive'
  	elsif (survey.active_since.blank? || survey.active_since <= now) &&
  		survey.active_until.present? && survey.active_until >= now
  		text = t('surveys.show.active_until', to: survey.active_until)
  	elsif survey.active_until.present? && survey.active_until < now
  	  text = t('surveys.show.survey_ended', date: survey.active_until)
      html_class += ' inactive'
    end

    text.present? ? content_tag(:h4, text, class: html_class) : nil
  end

  def partial_for_option(question_type, option)
    custom = option.custom? ? 'custom_' : ''
    "surveys/create/options/#{custom}#{question_type}"
  end
end