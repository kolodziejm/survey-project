class Autocomplete
  constructor: ->
    @groupAutocomplete()
    @userAutocomplete()

  groupAutocomplete: ->
    $('.group-autocomplete').each (index, elem) ->
      $(elem).tokenInput('/autocomplete/groups', {
        theme: 'facebook',
        preventDuplicates: true,
        prePopulate: $(elem).data('pre')
      })

  userAutocomplete: ->
    $('.user-autocomplete').each (index, elem) ->
      $(elem).tokenInput('/autocomplete/users', {
        theme: 'facebook',
        preventDuplicates: true,
        prePopulate: $(elem).data('pre')
      })

$ ->
  new Autocomplete