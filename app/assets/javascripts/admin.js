//= require common
//= require_tree ../../../vendor/assets/javascripts/sb-admin-2

$(document).ready(function(){
  setTimeout(function(){
    $('html').height($(document).height())
    $('body').height($(document).height())
    $('#wrapper').height($(document).height())
    $('#page-wrapper').height($(document).height())
  }, 200);
});