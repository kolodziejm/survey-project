class ContactForm
  constructor: (modal_selector) ->
    @form = $(modal_selector).find('form')
    @formUrl = @form.data('action')
    @initModal()
    @onSendTry()

  initModal: ->
    $('#contact-admin-link').click (e) ->
      $("#contact-modal").modal()

  onSendTry: ->
    _self = @

    @form.find('.send').click (e) ->
      e.preventDefault()

      body = $('#contact-body')
      email = $('#contact-email')

      if body.val().length > 0 && email.val().length > 0
        _self.send(email.val(), body.val())
      else
        _self.onError()

  send: (email, message) ->
    _self = @

    @form.find('.dialog-message').hide()

    $.ajax
      type:        'post'
      contentType: 'application/json; charset=utf-8'
      url:          _self.formUrl
      data: JSON.stringify(
        contact:
          body: message
          email: email
      )
    .done (response) ->
      _self.onSuccess()


  onError: ->
    @form.find('.dialog-error').show()

  onSuccess: ->
    @form.find('.dialog-success').show()

$ ->
  new ContactForm('#contact-modal')