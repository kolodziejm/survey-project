class window.Question
  constructor: (root_element) ->
    @root_element             = root_element
    @question_content_element = @root_element.find('.question-content')
    @options_element          = @root_element.find('.options')
    @question_type_input      = @root_element.find('.question-type')
    @observeTypeChange()
    @listenToOptionCollectionChanges()
    @createAddOptionLinksHash()
    @setType(@question_type_input.val())
    @updateOptionsOrder()
    @setupAddCustomLink()

  setupAddCustomLink: ->
    if @options_element.length
      if @options_element.find('.custom_text').length == 0
        @getCurrentAddCustomLink().show()
      else
        @getCurrentAddCustomLink().hide()

  observeTypeChange: ->
    _this = @
    _this.question_type_input.change( ->
      _this.setType $(@).val()
      _this.options_element.empty()
      _this.setupAddCustomLink()
      _this.getCurrentAddNormalLink().click() if _this.current_links
    )

  setType: (question_type) ->
    @enableCreateLinkForType(question_type)

  getCurrentAddCustomLink: ->
    @current_links.filter('.custom_text') if @current_links

  getCurrentAddNormalLink: ->
    @current_links.filter(':not(.custom_text)') if @current_links

  enableCreateLinkForType: (question_type) ->
    @current_links.remove() if @current_links
    @current_links = @option_links[question_type]
    @question_content_element.append(@current_links)

  createAddOptionLinksHash: ->
    _this = @
    _this.option_links = {}
    _this.question_type_input.children('option').each ->
      question_type = $(@).val()
      add_link = _this.root_element.find('*[data-option-type="'+question_type+'"]')
      _this.option_links[question_type] = add_link
      add_link.remove()

  updateOptionsOrder: ->
    _this = @
    _this.options_element.children('.option').each (index, option) ->
      $(option).find('input.option_position').val(index)

  listenToOptionCollectionChanges: ->
    _this = @
    @options_element.on('cocoon:after-insert', (e, insertedOption) ->
      _this.updateOptionsOrder()
      _this.getCurrentAddCustomLink().hide() if insertedOption.hasClass('custom_text')

    )
    @options_element.on('cocoon:before-remove', (e, insertedOption) ->
      _this.updateOptionsOrder()
      _this.getCurrentAddCustomLink().show() if insertedOption.hasClass('custom_text')
    )

