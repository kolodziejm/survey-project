class window.Section
  constructor: (root_element) ->
    @root_element      = root_element
    @questions_element = root_element.find('.questions')
    @questions         = []
    @initializeQuestions()
    @enableQuestionsSort()
    @listenToQuestionCollectionChanges()

  initializeQuestions: ->
    self = @
    $(@root_element).find('.question').each (index, question) ->
      self.questions.push new Question($(question))

  enableQuestionsSort: ->
    _this = @
    @questions_element.sortable({
      handle: '.drag-handle'
      forcePlaceholderSize: true
      placeholder: 'placeholder'
      tolerance: 'pointer'
      cursor: 'move'
      opacity: 0.65
      update: ->
        _this.updateQuestionsOrder()
    })
    @questions_element.disableSelection()

  updateQuestionsOrder: ->
    _this = @
    _this.questions_element.children('.question').each (index, question) ->
      $(question).find('input.question_position').val(index)

  listenToQuestionCollectionChanges: ->
    _this = @
    @questions_element.on('cocoon:after-insert', (e, insertedItem) ->
      _this.questions.push new Question(insertedItem)
      _this.updateQuestionsOrder()
    )
    @questions_element.on('cocoon:before-remove', (e, removedItem) ->
      removedQuestion = _this.questionFromElement(removedItem)
      _this.questions.remove(removedQuestion)
      _this.updateQuestionsOrder()
    )

  questionFromElement: (element) ->
    for question in @questions
      return question if question.root_element == element
