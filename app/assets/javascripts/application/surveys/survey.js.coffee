class window.Survey
  constructor: ->
    @root_element      = $('#survey')
    @sections_element  = @root_element.find('.sections')
    @sections          = []
    @initializeSections()
    @listenToSectionsCollectionChanges()
    @enableSectionsSort()
    @setPrivacySettings()
    @enablePrivacySwitch()

  initializeSections: ->
    self = @
    $('.section').each (index, section) ->
      self.sections.push new Section($(section))

  enableSectionsSort: ->
    _this = @
    @sections_element.sortable({
      handle: '.drag-handle'
      forcePlaceholderSize: true
      placeholder: 'placeholder'
      tolerance: 'pointer'
      cursor: 'move'
      opacity: 0.65
      update: ->
        _this.updateSectionsOrder()
    })
    @sections_element.disableSelection()

  listenToSectionsCollectionChanges: ->
    _this = @
    _this.sections_element.on('cocoon:after-insert', (e, insertedItem) ->
      _this.sections.push new Section(insertedItem)
      _this.updateSectionsOrder()
    )
    _this.sections_element.on('cocoon:before-remove', (e, removedItem) ->
      removedSection = _this.sectionFromElement(removedItem)
      _this.sections.remove(removedSection)
      _this.updateSectionsOrder()
    )

  updateSectionsOrder: ->
    _this = @
    _this.sections_element.children('.section').each (index, section) ->
     $(section).find('input.section_position').val(index)

  sectionFromElement: (element) ->
    for section in @sections
      return section if section.root_element == element

  setPrivacySettings: ->
    if $('#survey_public_false').is(':checked')
      $('#privacy-options').show()
    else
      $('#privacy-options').hide()

  enablePrivacySwitch: ->
    self = @
    $("input[name='survey[public]']").change ->
      self.setPrivacySettings()

jQuery ->
  new Survey