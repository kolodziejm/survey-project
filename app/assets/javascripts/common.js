//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require twitter/bootstrap
//= require bootstrap-typeahead
//= require bootstrap
//= require token_input
//= require autocomplete
//= require moment

//= require bootstrap-datetimepicker
//= require pickers
//= require jsapi
//= require chartkick