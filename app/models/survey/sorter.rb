class Survey
	class Sorter
    def initialize(relation, sort_field, sort_order)
      self.relation = relation
      self.sort_field = sort_field.to_sym
      self.sort_order = sort_order.to_sym
    end

    attr_accessor :relation, :sort_field, :sort_order

    BASIC_ATTRIBUTES = Survey.new.attributes.keys.map(&:to_sym)

    def sort
      if BASIC_ATTRIBUTES.include?(sort_field)
        relation.order("#{sort_field}" => sort_order)
      elsif sort_field == :owner_email
        relation.joins('JOIN users u ON u.id=surveys.owner_id').order("u.email #{sort_order}")
      else
        relation
      end
    end
  end
end