class QuestionGroup < ActiveRecord::Base
  has_many :questions, -> { order(position: :asc) },
    inverse_of: :question_group,
    dependent: :destroy
  belongs_to :survey, inverse_of: :question_groups

  accepts_nested_attributes_for :questions, allow_destroy: true

  validates :survey, presence: true
  validates :title, length: { maximum: 50 }
  validates :position, presence: true
  validates :questions, presence: { message: "Section must contain at least one question" }
end