class Group < ActiveRecord::Base
  has_and_belongs_to_many :users

  validates :name, uniqueness: true

  accepts_nested_attributes_for :users, allow_destroy: true

  def token_user_ids=(ids)
    self.user_ids = ids.first.split(',')
  end
end