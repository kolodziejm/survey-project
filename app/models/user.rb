class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  TYPES = %w(User SurveyOwner AdminUser)
  DELETED_USER_EMAIL = '_deleted@user.account'

  has_and_belongs_to_many :groups
  has_many :surveys, foreign_key: 'owner_id', inverse_of: :owner
  has_many :survey_answers, dependent: :destroy

  accepts_nested_attributes_for :groups, allow_destroy: true

  validates :first_name, length: { maximum: 50 }
  validates :last_name, length: { maximum: 50 }

  scope :without_deleted, -> { where("users.email != '#{DELETED_USER_EMAIL}'") }

  def self.deleted_user
    User.create_with(password: 'test1234', password_confirmation: 'test1234').find_or_create_by(email: DELETED_USER_EMAIL)
  end

  def deleted_user?
    email == DELETED_USER_EMAIL
  end

  def admin?
    is_a?(AdminUser)
  end

  def survey_owner?
    is_a?(SurveyOwner)
  end

  def token_group_ids=(ids)
    self.group_ids = ids.first.split(',')
  end

  def autocomplete_format
    { id: id, name: "#{first_name} #{last_name} #{email}" }
  end
end
