class Question < ActiveRecord::Base
  enum question_type: {
    text: 0,
    single_choice: 1,
    multiple_choice: 2,
    date: 3,
    time: 4,
    date_time: 5,
    text_area: 6,
    number: 7
  }

  has_many :options, inverse_of: :question, dependent: :destroy
  has_many :question_answers, dependent: :destroy
  has_many :option_answers, through: :question_answers
  belongs_to :question_group, inverse_of: :questions

  accepts_nested_attributes_for :options, allow_destroy: true

  validates :question_type, presence: true
  validates :question_group, presence: true
  validates :text, presence: true
  validates :position, presence: true

  validate :validate_options_count,
           if: :options_required?

  private

  def options_required?
    %w(single_choice multiple_choice).include?(question_type)
  end

  def validate_options_count
    errors.add(:options, "Question requires at least #{min_options_count} options") if options.select { |o| !o.marked_for_destruction? }.size < min_options_count
  end

  def min_options_count
    if question_type=='single_choice'
      2
    elsif question_type=='multiple_choice'
      1
    else
      0
    end
  end
end