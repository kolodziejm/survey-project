class QuestionAnswer < ActiveRecord::Base
  belongs_to :survey_answer
  belongs_to :question

  has_many :option_answers, dependent: :destroy

  has_many :options,
           through: :option_answers

  validates :survey_answer,
            presence: true

  validates :option_answers,
            presence: { message: 'This question requires a choice' }, if: :options_required?

  validate :validate_answer

  private

  def options_required?
    %w(single_choice).include?(question.question_type)
  end

  def validate_answer
    Validator.new(self).validate
  end
end