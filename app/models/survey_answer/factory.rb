class SurveyAnswer
  class Factory
    class UnknownAnswerType < ArgumentError; end

    def initialize(params)
      self.survey = params[:survey]
      self.user   = params[:user]
      self.data   = params[:data]
    end

    def build
      self.survey_answer = SurveyAnswer.new(survey: survey, user: user)

      data[:question_answers].each do |question_id, answer_hash|
        build_answer(survey.questions.find(question_id), answer_hash)
      end

      survey_answer
    end

    private

    attr_accessor :survey, :user, :data, :survey_answer

    def build_answer(question, answer_hash)
      case question.question_type
      when 'text','text_area', 'date', 'time', 'date_time', 'number'
        build_text_answer(question, answer_hash)
      when 'single_choice'
        build_single_choice_answer(question, answer_hash)
      when 'multiple_choice'
        build_multiple_choice_answer(question, answer_hash)
      else
        raise UnknownAnswerType, question.question_type
      end
    end

    def build_text_answer(question, answer_hash)
      survey_answer.question_answers.build(text: answer_hash[:text], question_id: question.id) unless answer_hash[:text].blank?
    end

    def build_single_choice_answer(question, answer_hash)
      selected_option = question.options.find(answer_hash[:options])
      custom_text = answer_hash.fetch(:custom_text, nil)

      option_hash = { option_id: selected_option.id }
      option_hash.merge!({ custom_text: custom_text }) if custom_text

      survey_answer.question_answers.build(question_id: question.id).option_answers.build(option_hash)
    end

    def build_multiple_choice_answer(question, answer_hash)
      selected_options = question.options.find(answer_hash[:options].delete_if { |v| v.empty? })
      custom_text = answer_hash.fetch(:custom_text, nil)

      question_answer = survey_answer.question_answers.build(question_id: question.id)
      selected_options.each do |selected_option|
        option_hash = { option_id: selected_option.id }
        option_hash.merge!({ custom_text: custom_text }) if custom_text
        question_answer.option_answers.build(option_hash)
      end
    end
  end
end