class SurveyAnswer
	class Sorter
    def initialize(relation, sort_field, sort_order)
      self.relation = relation
      self.sort_field = sort_field.to_sym
      self.sort_order = sort_order.to_sym
    end

    attr_accessor :relation, :sort_field, :sort_order

    BASIC_ATTRIBUTES = SurveyAnswer.new.attributes.keys.map(&:to_sym)

    def sort
      if BASIC_ATTRIBUTES.include?(sort_field)
        relation.order("#{sort_field}" => sort_order)
      elsif sort_field == :survey_title
        relation.joins('JOIN surveys s ON s.id=survey_answers.survey_id').order("s.title #{sort_order}")
      else
        relation
      end
    end
  end
end