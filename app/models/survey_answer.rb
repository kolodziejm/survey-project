class SurveyAnswer < ActiveRecord::Base
  has_many :question_answers, dependent: :destroy
  has_many :option_answers, through: :question_answers

  belongs_to :survey
  belongs_to :user

  validates :user,
            presence: true

  validates :survey,
            presence: true

  def survey
    Survey.unscoped.where(id: survey_id).first
  end

  validate :validate_survey_ongoing
  validate :validate_total_answer_limit

  scope :order_by, -> (field, order = :asc) do
    Sorter.new(all, field, order).sort
  end

  private

  def validate_survey_ongoing
    if !survey.ongoing?
      errors.add(:question_answers, :survey_inactive)
    end
  end

  def validate_total_answer_limit
    if survey.total_answer_limit_reached?
      errors.add(:question_answers, :total_answer_limit_reached)
    end
  end

  def validate_answer_limit_per_user
    if survey.answer_limit_per_user_reached?
      errors.add(:question_answers, :answer_limit__per_user_reached)
    end
  end
end