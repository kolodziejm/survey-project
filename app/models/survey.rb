class Survey < ActiveRecord::Base
  belongs_to :owner, class_name: 'User', inverse_of: :surveys

  default_scope { where(:deleted_at => nil) }

  has_many :question_groups,
    inverse_of: :survey,
    dependent: :destroy
  has_many :questions,
    through: :question_groups,
    dependent: :destroy
  has_many :survey_answers, dependent: :destroy

  has_and_belongs_to_many :groups
  alias_method :allowed_groups, :groups

  has_and_belongs_to_many :users
  alias_method :allowed_users, :users

  accepts_nested_attributes_for :question_groups, allow_destroy: true

  validates :title, presence: true, length: { maximum: 50 }
  validates :owner, presence: true
  validates :question_groups, presence: { message: "Survey must contain at least one section" }
  validate :validate_activity_period

  scope :answerable_by, ->(user) {
    ids = select(:id)
    .joins("LEFT JOIN surveys_users ON surveys_users.survey_id = surveys.id LEFT JOIN users ON users.id = surveys_users.user_id
           LEFT JOIN groups_surveys ON groups_surveys.survey_id = surveys.id LEFT JOIN groups ON groups.id = groups_surveys.group_id
           LEFT JOIN groups_users ON groups_users.group_id = groups_surveys.group_id")
    .where("public = true
            OR surveys_users.user_id = #{user.id}
            OR groups_users.user_id = #{user.id}")
    .distinct

    where(id: ids)
  }

  scope :active, -> do
    where(
      "COALESCE(active_since <= :now, TRUE) AND COALESCE(active_until >= :now, TRUE)",
      now: DateTime.now
    )
  end

  scope :editable_by, ->(user) {
    return if user.admin?

    where(owner: user)
  }

  scope :order_by, -> (field, order = :asc) do
    Sorter.new(all, field, order).sort
  end

  attr_reader :token_user_ids, :token_group_ids

  def ongoing?
    (active_since.blank? || active_since <= DateTime.now) &&
      (active_until.blank? || active_until >= DateTime.now)
  end

  def close
    if ongoing?
      update_attribute(:active_until, DateTime.now)
    end
  end

  def total_answer_limit_reached?
    max_answers.present? && survey_answers.count >= max_answers
  end

  def answer_limit_per_user_reached?(user_id)
    max_answers_per_user.present? && survey_answers.where(user_id: user_id).count >= max_answers_per_user
  end

  def token_user_ids=(ids)
    self.user_ids = ids.split(',')
  end

  def token_group_ids=(ids)
    self.group_ids = ids.split(',')
  end

  def editable_by?(user)
    owner == user || user.admin?
  end

  def answerable_by?(user)
    self.public || user_allowed?(user) || user_in_allowed_groups?(user)
  end

  def self.initializable_by?(user)
    user && (user.admin? || user.survey_owner?)
  end

  private

  def user_allowed?(user)
    allowed_users.include?(user)
  end

  def user_in_allowed_groups?(user)
    (allowed_groups & user.groups).present?
  end

  def validate_activity_period
    if active_since.present? && active_until.present? && active_since > active_until
      errors.add(:active_since, :must_be_before_active_until)
    end
  end
end