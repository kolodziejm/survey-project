class OptionAnswer < ActiveRecord::Base
  belongs_to :question_answer, inverse_of: :option_answers
  belongs_to :option, inverse_of: :option_answers

  validates :question_answer,
            presence: true

  validates :option,
            presence: true
end