class Option < ActiveRecord::Base
  belongs_to :question, inverse_of: :options
  has_many :option_answers, inverse_of: :option, dependent: :destroy

  validates :question, presence: true
  validates :position, presence: true
  validates :text, presence: true
end