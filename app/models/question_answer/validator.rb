class QuestionAnswer
  class Validator
    def initialize(answer)
      self.answer = answer
    end

    def validate
      case answer.question.question_type
      when 'date', 'time', 'date_time'
        validate_date_time
      when 'number'
        validate_number
      end
    end

    private

    attr_accessor :answer

    def validate_date_time
      if answer.text.present? && answer.text.to_time_safe.nil?
        answer.errors.add(:text, :invalid_format)
      end
    end

    def validate_number
      if answer.text.present? && answer.text != '0' && answer.text.to_i == 0
        answer.errors.add(:text, :must_be_a_number)
      end
    end
  end
end