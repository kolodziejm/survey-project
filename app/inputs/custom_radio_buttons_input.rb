class CustomRadioButtonsInput < SimpleForm::Inputs::CollectionRadioButtonsInput
  def input
    iopts = { item_wrapper_class: 'radio' }
    return @builder.send(
      "collection_radio_buttons",
      attribute_name,
      collection,
      value_method,
      label_method,
      iopts,
      input_html_options,
      &collection_block_for_nested_boolean_style
    )
  end

  protected

  def build_nested_boolean_style_item_tag(collection_builder)
    tag = String.new
    tag << collection_builder.radio_button(checked: is_checked?(collection_builder.object.send(value_method))) + label_content(collection_builder)
    return tag.html_safe
  end

  private

  def label_content(builder)
    builder.object.custom? ? (t('surveys.show.other') + ':' + @builder.text_field(:custom_text, class: 'form-control', placeholder: t('surveys.show.placeholders.text'))).html_safe : builder.text
  end

  def label_method
    options[:label_method]
  end

  def value_method
    options[:value_method]
  end

  def is_checked?(id)
    checked = Array.wrap(options[:checked])
    checked.include?(id)
  end
end