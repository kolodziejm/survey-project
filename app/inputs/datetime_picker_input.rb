class DatetimePickerInput < SimpleForm::Inputs::StringInput
  def input(wrapper_options)
    value = object.send(attribute_name) if object.respond_to? attribute_name
    display_pattern = I18n.t('datepicker.dformat', :default => '%d/%m/%Y') + ' ' + I18n.t('timepicker.dformat', :default => '%R')
    input_html_options[:value] ||= format_value(value, display_pattern)

    input_html_options[:type] = 'text'
    picker_pettern = I18n.t('datepicker.pformat', :default => 'DD/MM/YYYY') + ' ' + I18n.t('timepicker.pformat', :default => 'HH:mm')
    input_html_options[:data] ||= {}
    input_html_options[:data].merge!({ date_format: picker_pettern, date_language: I18n.locale.to_s,
                                       date_weekstart: I18n.t('datepicker.weekstart', :default => 0) })

    template.content_tag :div, class: 'input-group date datetimepicker' do
      input = template.content_tag :span, class: 'input-group-btn' do
        template.content_tag :button, class: 'btn btn-default', type: 'button' do
          template.content_tag :span, '', class: 'fa fa-calendar'
        end
      end
      input + super(wrapper_options) # leave StringInput do the real rendering
    end
  end

  def input_html_classes
    super.push ''   # 'form-control'
  end

  def format_value(value, display_pattern)
    return if value.blank?

    if value.is_a?(String)
      I18n.localize(value.to_time_safe, :format => display_pattern) if !value.to_time_safe.nil?
    else
      I18n.localize(value, :format => display_pattern)
    end
  end
end
