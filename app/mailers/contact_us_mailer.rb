class ContactUsMailer < ActionMailer::Base
  def contact_us(options)
    email = options[:email]
    @body = options[:body]

    mail(from: email, to: AdminUser.first.email, subject: 'SurveyCreator: user question')
  end
end