class SurveysController < ApplicationController
  include SurveyFilters

  before_filter :disallow_guest
  before_filter :initialize_and_authorize_survey_for_edit, only: [:edit, :update, :destroy, :close]
  before_filter :initialize_and_authorize_survey_for_answer, only: [:show]

  helper_method :sort_field, :sort_order

  def index
    @surveys = Survey.includes(:owner).answerable_by(current_user).active.order_by(sort_field, sort_order).paginate(page: page, per_page: per_page)
  end

  def new
    initialize_new_survey
  end

  def create
    @survey = current_user.surveys.new(survey_params)

    if !@survey.save
      render :new
      flash[:error] = I18n.t('surveys.create.failure')

      return
    end
  end

  def show
    @answer = @survey.survey_answers.build(user: current_user)
  end

  def edit
  end

  def update
    if @survey.update(survey_params)
      flash[:success] = I18n.t('surveys.update.success')
      redirect_to edit_survey_path(@survey)
    else
      flash[:error] = I18n.t('surveys.update.failure')
      render :edit
    end
  end

  def destroy
    @survey.update_attribute(:deleted_at, DateTime.now)
    flash[:success] = I18n.t('surveys.destroy.success')

    redirect_to manage_surveys_path
  end

  def manage
    @surveys = Survey.editable_by(current_user).order(created_at: :desc).paginate(page: params[:page])
  end

  def close
    @survey.close
    flash[:success] = I18n.t('surveys.close.success')

    redirect_to manage_surveys_path
  end

  private

  def initialize_new_survey
    raise CanCan::AccessDenied unless Survey.initializable_by?(current_user)

    @survey = current_user.surveys.new
    @survey.question_groups.build(position: 0)
    @survey.question_groups.first.questions.build(question_type: :text, position: 0)
  end

  def survey_params
    params.require(:survey).permit(:title, :public, :token_group_ids, :token_user_ids, :active_since, :active_until, :max_answers, :max_answers_per_user,
      question_groups_attributes: [:id, :_destroy, :title, :position, questions_attributes:
        [:id, :_destroy, :text, :question_type, :position, :mandatory, options_attributes:
          [:id, :_destroy, :text, :position, :custom]
        ]
      ]
    )
  end

  def sort_field
    params[:sort_field] || :created_at
  end

  def sort_order
    params[:sort_order] || :desc
  end
end