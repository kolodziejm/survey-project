module Admin
  class UsersController < BaseController
    before_action :set_user, only: [:edit, :update, :destroy]

    def index
      @users = User.paginate(page: page, per_page: per_page)
    end

    def new
      @user = User.new
    end

    def edit
    end

    def create
      @user = User.create(user_params)
      if @user.valid?
        redirect_to edit_admin_user_path(@user), notice: I18n.t('admin.users.created') and return
      else
        render :new
      end
    end

    def update
      if @user.update(user_params)
        redirect_to edit_admin_user_path(@user), notice: I18n.t('admin.users.updated') and return
      else
        render :edit
      end
    end

    def destroy
      User.transaction do
        @user.surveys.update_all(owner_id: User.deleted_user)
        @user.survey_answers.update_all(user_id: User.deleted_user)
        @user.destroy
      end

      redirect_to admin_users_path, notice: I18n.t('admin.users.deleted')
    end

    private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params[:user] ||= params[:survey_owner] || params[:admin_user]
      params.require(:user).permit(:first_name, :last_name, :user, :password, :password_confirmation, :type, :email, { token_group_ids: [] })
    end
  end
end