module Admin
  class GroupsController < BaseController
    before_action :set_group, only: [:edit, :update, :destroy]

    def index
      @groups = Group.paginate(page: page, per_page: per_page)
    end

    def new
      @group = Group.new
    end

    def create
      @group = Group.new(group_params)

      if @group.save
        redirect_to admin_groups_path, notice: I18n.t('admin.groups.created')
      else
        render :new
      end
    end

    def edit
    end

    def update
      if @group.update(group_params)
        redirect_to admin_groups_path, notice: I18n.t('admin.groups.updated')
      else
        render :edit
      end
    end

    def destroy
      @group.destroy
      redirect_to admin_groups_path, notice: I18n.t('admin.groups.deleted')
    end

    private

    def set_group
      @group = Group.find(params[:id])
    end

    def group_params
      params.require(:group).permit(:name, { token_user_ids: [] })
    end
  end
end