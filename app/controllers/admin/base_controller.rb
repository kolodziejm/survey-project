class Admin::BaseController < ApplicationController
  layout 'admin'

  before_filter :authorize_user

  private

  def authorize_user
    raise CanCan::AccessDenied unless current_user.try(:admin?)
  end
end