class YourAnswersController < ApplicationController
  before_filter :disallow_guest

  def index
    @answers = current_user.survey_answers.includes(:survey).order_by(sort_field, sort_order).paginate(page: page, per_page: per_page)
  end

  def show
    @answer = collection.find(params[:id])

    @survey = @answer.survey
  end

  private

  def collection
    current_user.survey_answers
  end

  helper_method :sort_field, :sort_order

  def sort_field
    params[:sort_field] || :created_at
  end

  def sort_order
    params[:sort_order] || :desc
  end
end