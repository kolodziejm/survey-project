class ContactController < ApplicationController
  def create
    ContactUsMailer.contact_us(body: params[:contact][:body], email: params[:contact][:email])

    head :ok
  end
end