class AnswersController < ApplicationController
  include SurveyFilters

  before_filter :disallow_guest
  before_filter :initialize_and_authorize_survey_for_edit, only: [:index, :show]
  before_filter :initialize_and_authorize_survey_for_answer, only: [:create]

  def index
  end

  def show
    @answer = @survey.survey_answers.find(params[:id])
  end

  def create
    @answer = SurveyAnswer::Factory.new(user: current_user, data: params[:survey_answer], survey: @survey).build

    if @answer.save
      redirect_to surveys_path, notice: I18n.t('answers.create.success')
    else
      flash[:error] = I18n.t('answers.create.failure')
      render 'surveys/show'
    end
  end
end