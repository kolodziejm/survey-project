class ProfilesController < ApplicationController
  before_filter :disallow_guest
  before_filter :set_user

  def edit
  end

  def update
    if @user.update(user_params)
      flash[:success] = I18n.t('updated')
      redirect_to profile_path
    else
      render :edit
    end
  end

  private

  def set_user
    @user = current_user
  end

  def user_params
    params.require(user_type).permit(:email, :first_name, :last_name)
  end

  def user_type
    current_user.type.underscore.to_sym rescue :user
  end
end