module SurveyFilters
  def initialize_and_authorize_survey_for_answer
    @survey = Survey.find(params[:survey_id] || params[:id])

    unless @survey.answerable_by?(current_user)
      raise CanCan::AccessDenied
    end
  end

  def initialize_and_authorize_survey_for_edit
    @survey = Survey.find(params[:survey_id] || params[:id])

    unless @survey.editable_by?(current_user)
      raise CanCan::AccessDenied
    end
  end
end