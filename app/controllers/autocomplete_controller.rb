class AutocompleteController < ApplicationController
  DEFAULT_LIMIT = 10

  def groups
    render json: Group.where("name ILIKE ?", "%#{params[:q]}%").limit(DEFAULT_LIMIT).to_json
  end

  def users
    render json: User.where("first_name ILIKE :q OR last_name ILIKE :q OR email ILIKE :q", q: "%#{params[:q]}%")
                .limit(DEFAULT_LIMIT).map(&:autocomplete_format).to_json
  end
end