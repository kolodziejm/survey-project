class String
  def to_time_safe
    to_time
  rescue ArgumentError
    nil
  end
end