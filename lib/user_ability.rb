class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user || User.new

    case user
    when AdminUser
      admin_abilites
    when SurveyOwner
      survey_owner_abilites
    when User
      user_abilites
    else
      guest_abilites
    end
  end

  private

  def admin_abilites
    can :manage, :all
  end
end
