# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150921200036) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "groups", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups", ["name"], name: "index_groups_on_name", unique: true, using: :btree

  create_table "groups_surveys", id: false, force: true do |t|
    t.integer "survey_id"
    t.integer "group_id"
  end

  add_index "groups_surveys", ["group_id"], name: "index_groups_surveys_on_group_id", using: :btree
  add_index "groups_surveys", ["survey_id", "group_id"], name: "index_groups_surveys_on_survey_id_and_group_id", unique: true, using: :btree
  add_index "groups_surveys", ["survey_id"], name: "index_groups_surveys_on_survey_id", using: :btree

  create_table "groups_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups_users", ["group_id"], name: "index_groups_users_on_group_id", using: :btree
  add_index "groups_users", ["user_id"], name: "index_groups_users_on_user_id", using: :btree

  create_table "option_answers", force: true do |t|
    t.integer "question_answer_id", null: false
    t.integer "option_id",          null: false
    t.string  "custom_text"
  end

  add_index "option_answers", ["option_id", "question_answer_id"], name: "index_option_answers_on_option_id_and_question_answer_id", unique: true, using: :btree

  create_table "options", force: true do |t|
    t.string   "text"
    t.integer  "position",                    null: false
    t.integer  "question_id",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "custom",      default: false, null: false
  end

  add_index "options", ["position"], name: "index_options_on_position", using: :btree

  create_table "question_answers", force: true do |t|
    t.integer "survey_answer_id", null: false
    t.integer "question_id",      null: false
    t.text    "text"
  end

  create_table "question_groups", force: true do |t|
    t.string   "title",      limit: 50
    t.integer  "position",              null: false
    t.integer  "survey_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "question_groups", ["position"], name: "index_question_groups_on_position", using: :btree

  create_table "questions", force: true do |t|
    t.string   "text"
    t.boolean  "mandatory",         default: false
    t.integer  "question_type",                     null: false
    t.integer  "position",                          null: false
    t.integer  "question_id"
    t.integer  "question_group_id",                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "questions", ["position"], name: "index_questions_on_position", using: :btree

  create_table "survey_answers", force: true do |t|
    t.datetime "answered_at"
    t.integer  "user_id",     null: false
    t.integer  "survey_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "surveys", force: true do |t|
    t.string   "title",                limit: 50
    t.integer  "owner_id"
    t.datetime "active_since"
    t.datetime "active_until"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public",                          default: true, null: false
    t.integer  "max_answers"
    t.integer  "max_answers_per_user"
    t.datetime "deleted_at"
  end

  create_table "surveys_users", id: false, force: true do |t|
    t.integer "survey_id"
    t.integer "user_id"
  end

  add_index "surveys_users", ["survey_id", "user_id"], name: "index_surveys_users_on_survey_id_and_user_id", unique: true, using: :btree
  add_index "surveys_users", ["survey_id"], name: "index_surveys_users_on_survey_id", using: :btree
  add_index "surveys_users", ["user_id"], name: "index_surveys_users_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.string   "first_name",             limit: 50
    t.string   "last_name",              limit: 50
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "groups_surveys", "groups", name: "groups_surveys_group_id_fk", dependent: :delete
  add_foreign_key "groups_surveys", "surveys", name: "groups_surveys_survey_id_fk", dependent: :delete

  add_foreign_key "groups_users", "groups", name: "groups_users_group_id_fk", dependent: :delete
  add_foreign_key "groups_users", "users", name: "groups_users_user_id_fk", dependent: :delete

  add_foreign_key "option_answers", "options", name: "option_answers_option_id_fk"
  add_foreign_key "option_answers", "question_answers", name: "option_answers_question_answer_id_fk"

  add_foreign_key "options", "questions", name: "options_question_id_fk"

  add_foreign_key "question_answers", "questions", name: "question_answers_question_id_fk"
  add_foreign_key "question_answers", "survey_answers", name: "question_answers_survey_answer_id_fk"

  add_foreign_key "question_groups", "surveys", name: "question_groups_survey_id_fk"

  add_foreign_key "questions", "question_groups", name: "questions_question_group_id_fk"

  add_foreign_key "survey_answers", "surveys", name: "survey_answers_survey_id_fk"
  add_foreign_key "survey_answers", "users", name: "survey_answers_user_id_fk"

  add_foreign_key "surveys_users", "surveys", name: "surveys_users_survey_id_fk", dependent: :delete
  add_foreign_key "surveys_users", "users", name: "surveys_users_user_id_fk", dependent: :delete

end
