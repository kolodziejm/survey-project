class EnabledSurveySoftDelete < ActiveRecord::Migration
  def change
    add_column :surveys, :deleted_at, :datetime
  end
end
