class AddSurveyModel < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.string :title, limit: 50
      t.belongs_to :owner, class_name: 'User', foreign_key: :owner_id
      t.datetime :active_since
      t.datetime :active_until

      t.timestamps
    end
  end
end
