class CreateOptionAnswer < ActiveRecord::Migration
  def change
    create_table :option_answers do |t|
      t.references :question_answer, null: false
      t.references :option, null: false
    end

    add_index :option_answers, [:option_id, :question_answer_id], unique: true

    add_foreign_key :option_answers, :question_answers
    add_foreign_key :option_answers, :options
  end
end
