class RemoveSurveyPrivacySetting < ActiveRecord::Migration
  def change
    drop_table :survey_privacy_settings
  end
end
