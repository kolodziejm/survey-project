class CreateSurveyAnswer < ActiveRecord::Migration
  def change
    create_table :survey_answers do |t|
      t.datetime :answered_at
      t.references :user, null: false
      t.references :survey, null: false

      t.timestamps
    end

    add_foreign_key :survey_answers, :users
    add_foreign_key :survey_answers, :surveys
  end
end
