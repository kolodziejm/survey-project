class CreateGroupsSurveys < ActiveRecord::Migration
  def change
    create_table :groups_surveys, id: false do |t|
      t.references :survey
      t.references :group
    end

    add_index :groups_surveys, :survey_id
    add_index :groups_surveys, :group_id
    add_index :groups_surveys, [:survey_id, :group_id], unique: true
    add_foreign_key :groups_surveys, :surveys, dependent: :delete
    add_foreign_key :groups_surveys, :groups, dependent: :delete
  end
end
