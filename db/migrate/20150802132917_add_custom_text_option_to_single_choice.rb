class AddCustomTextOptionToSingleChoice < ActiveRecord::Migration
  def change
    add_column :option_answers, :custom_text, :string
    add_column :options, :custom, :bool, default: false, null: false
  end
end
