class AddPublicToSurvey < ActiveRecord::Migration
  def change
    add_column :surveys, :public, :boolean, default: true, null: false
  end
end
