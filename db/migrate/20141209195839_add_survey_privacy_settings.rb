class AddSurveyPrivacySettings < ActiveRecord::Migration
  def change
    create_table :survey_privacy_settings do |t|
      t.references :survey, null: false
      t.integer    :privacy_object_id, null: false
      t.string     :privacy_object_type, null: false
      t.boolean    :readable, default: false, null: false
      t.boolean    :writable, default: false, null: false
    end

    add_index :survey_privacy_settings, :survey_id
    add_index :survey_privacy_settings,
              [:privacy_object_id, :privacy_object_type],
              unique: true,
              name:   'index_survey_privacy_settings_on_object_id_and_object_type'

    add_foreign_key :survey_privacy_settings, :surveys
  end
end
