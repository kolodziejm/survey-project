class AddAnswerLimit < ActiveRecord::Migration
  def change
    add_column :surveys, :max_answers, :integer, default: nil
    add_column :surveys, :max_answers_per_user, :integer, default: nil
  end
end
