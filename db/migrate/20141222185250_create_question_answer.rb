class CreateQuestionAnswer < ActiveRecord::Migration
  def change
    create_table :question_answers do |t|
      t.references :survey_answer, null: false
      t.references :question, null: false
      t.text :text
    end

    add_foreign_key :question_answers, :survey_answers
    add_foreign_key :question_answers, :questions
  end
end
