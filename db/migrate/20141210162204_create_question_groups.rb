class CreateQuestionGroups < ActiveRecord::Migration
  def change
    create_table :question_groups do |t|
      t.string     :title, limit: 50
      t.integer    :position, null: false
      t.references :survey

      t.timestamps
    end

    add_index :question_groups, :position
    add_foreign_key :question_groups, :surveys
  end
end
