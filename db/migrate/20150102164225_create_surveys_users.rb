class CreateSurveysUsers < ActiveRecord::Migration
  def change
    create_table :surveys_users, id: false do |t|
      t.references :survey
      t.references :user
    end

    add_index :surveys_users, :survey_id
    add_index :surveys_users, :user_id
    add_index :surveys_users, [:survey_id, :user_id], unique: true
    add_foreign_key :surveys_users, :users, dependent: :delete
    add_foreign_key :surveys_users, :surveys, dependent: :delete
  end
end
