class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.string     :text
      t.integer    :position, null: false
      t.references :question, null: false

      t.timestamps
    end

    add_index :options, :position
    add_foreign_key :options, :questions
  end
end
