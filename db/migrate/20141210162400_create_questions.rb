class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string     :text
      t.boolean    :mandatory, default: false
      t.integer    :question_type, null: false
      t.integer    :position, null: false
      t.references :question
      t.references :question_group, null: false

      t.timestamps
    end

    add_index :questions, :position
    add_foreign_key :questions, :question_groups
  end
end
