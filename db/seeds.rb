# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
passwords_hash = { password: 'test1234', password_confirmation: 'test1234' }

User.create(passwords_hash.merge(email: 'user@example.com') )
AdminUser.create(passwords_hash.merge(email: 'admin@example.com') )
SurveyOwner.create(passwords_hash.merge(email: 'survey_owner@example.com') )