Rails.application.routes.draw do
  root to: 'home#index'

  devise_for :users

  namespace :admin do
    root to: 'statistics#index'

    resources :users
    resources :groups, except: [:show]
    resources :statistics, only: [:index]
  end

  resources :surveys do
    collection do
      get 'manage', to: :manage
    end
    put :close
    resources :answers, only: [:index, :show, :create]
  end

  resources :your_answers, only: [:index, :show]

  post 'contact', to: 'contact#create'

  get 'profile', to: 'profiles#edit'
  put 'profile', to: 'profiles#update'

  get 'autocomplete/groups', to: 'autocomplete#groups'
  get 'autocomplete/users', to: 'autocomplete#users'
end
